Unicode data fetch
==================

Various projects depend on the Unicode data set provided by the Unicode
Consortium, and available at:

  https://www.unicode.org/Public/UNIDATA/

This module downloads that data and installs it in a well-known location
that can be reliably used by other projects.
