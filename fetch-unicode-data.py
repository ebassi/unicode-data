#!/usr/bin/env python3

# Copyright 2019  Emmanuele Bassi
# SPDX-License-Identifier: MIT

import argparse
import os
import shutil
import urllib.request
import zipfile

UCD_FILE = 'UCD.zip'
UCD_URL = 'https://www.unicode.org/Public/UNIDATA/' + UCD_FILE

ap = argparse.ArgumentParser(description='Download and extract Unicode data')
ap.add_argument('--out-dir', metavar='DIRECTORY', help='Output directory')
args = ap.parse_args()

try:
    os.mkdir(args.out_dir)
except FileExistsError:
    pass

with urllib.request.urlopen(UCD_URL) as response, open(UCD_FILE, 'wb') as out_file:
    shutil.copyfileobj(response, out_file)

with zipfile.ZipFile(UCD_FILE, 'r') as zip_ref:
    zip_ref.extractall(path=args.out_dir)
